#      ASTRA IPTV SERVER WATCHDOG VERSION 1.2
#      Need libaries:  nc, busybox, perl, perl CA::Mozilla
#      Author: Khartumov Alikhan aka Gamani
##########################################################
###############FROM               AUTHOR##################
##  If you need, you can change this script to any      ##
##  process, but this process need opened WEB port, to  ##
##  watch the process activity.                         ##
##  If script NOT working, you can use BASH Debugger:   ##
##                bash -x astrachecker.sh               ##
##  My DEBUG KEYs, but it need uncomment, if you want   ##
##  to work with is.                                    ##
##########################################################
#BINARIES
#! /bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
PATH=$PATH:/usr/bin/perl
export PATH
#BINARIES END
#VARIABELS
f=0
c=0
#Main First Astra IPTV server checker. Its based by see worked Astra IPTV WEB Port (8080)
/bin/nc -vz 127.0.0.1 8080
if [ $? -eq 0 ]; then
    echo OK
else
    f=1
    echo Astra is NOT WORKING!!!
    /usr/bin/perl /root/pushmsg.pl
    sh astrastart.sh
fi
#Main First checker END
#Sleep
###DEBUG KEY
#echo "$f"
#echo "$c"
############
sleep 5s
#Sleep END
#Second Astra checker, its work ONLY if Astra server is turned OFF in Main First checking
if [ "$c" -ne 0 ]; then
    /bin/nc -vz 127.0.0.1 8080
    if [ $? -eq 0 ]; then
        echo OK! Server started succesfully!
        f=0
        /usr/bin/perl /root/pushstarted.pl
    else
        echo CRITICAL ERROR WHILE RUNNING ASTRA SERVER!!!
        /usr/bin/perl /root/pushcrit.pl
    fi
else
    echo Server Already Working!
fi
###DEBUG KEY
#echo "$f"
#echo "$c"
############
